from flask import Flask, render_template, request, jsonify, redirect, url_for, session, make_response, Response
from flask_cors import CORS, cross_origin
import os
from cv2 import *
import numpy as np
import os
import random
from celery import Celery
import time
import tensorflow as tf
from werkzeug.utils import secure_filename
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
import contextlib
from functools import wraps
from scipy.spatial import distance as dist

APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top

app = Flask(__name__)
maskUse=0.0
maskNoUse=0.0
CORS(app)
cascPath = os.path.dirname(
    cv2.__file__) + "/data/haarcascade_frontalface_alt2.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
model = load_model("mask_recog1.h5")

def progress_helper():
    # faking progess
    millis = int(round(time.time() * 1000))
    progress = (millis / ((1584307819064)+4400000000000))
    return progress

def gen(camera):
    capture = cv2.VideoCapture(0)
    current_frame = 0
    while True:
        sucess, frame = capture.read()
        if(sucess):
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = faceCascade.detectMultiScale(gray,
                                                scaleFactor=1.1,
                                                minNeighbors=5,
                                                minSize=(60, 60),
                                                flags=cv2.CASCADE_SCALE_IMAGE)
            faces_list=[]
            preds=[]
            maskPerson=0
            noMaskPerson=0
            facesPos=[]
            for (x, y, w, h) in faces:
                face_frame = frame[y:y+h,x:x+w]
                face_frame = cv2.cvtColor(face_frame, cv2.COLOR_BGR2RGB)
                face_frame = cv2.resize(face_frame, (224, 224))
                face_frame = img_to_array(face_frame)
                face_frame = np.expand_dims(face_frame, axis=0)
                face_frame =  preprocess_input(face_frame)
                faces_list.append(face_frame)
                if len(faces_list)>0:
                    preds = model.predict(faces_list)
                for pred in preds:
                    (mask, withoutMask) = pred
                label = "Mask" if mask > withoutMask else "No Mask"
                masked = 1 if mask > withoutMask else 0
                print(label)
                print(mask)
                print(withoutMask)
                maskUse=mask
                maskNoUse=withoutMask
                color = (0, 255, 0) if label == "Mask" else (0, 0, 255)
                #label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)
        
                D = dist.euclidean((x, y), (x + w, y + h))
                print(D)
                if len(faces_list)>1:
                    facesPos.append((x, y))

                
            
            if len(facesPos)>1:
                d1 =facesPos.pop()
                d2 =facesPos.pop()    
                D = dist.euclidean(d1, d2)
                print(D) 
            frame_name = "framevideo.jpg"
            #extracting frames from video
            cv2.imwrite(frame_name, frame) 
            
            frame = open(frame_name,'rb').read()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed', methods=['GET'])
def video_feed():
    pathVideo = f"{APP_ROOT}/static/video/site.mp4"
    return Response(gen(pathVideo), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')


@app.route('/user', methods=['GET'])
def get_user_account_info():
    user = {"Name": "Sidney François"}
    response = jsonify(user)
    return response


@app.route('/ontime', methods=['GET'])
def ontime():
    dict = {"ontime":str(progress_helper()*.70)}
    return jsonify(dict)

@app.route('/progress', methods=['GET'])
def progress():
    dict = {"progress":str(progress_helper())}
    return jsonify(dict)

@app.route('/defects',methods=['GET'])
def defects():
    defects = {"1":{"Type": "",
                    "Description": "",
                    "Importance": withoutMask,
                    "Priority": mask,
                    "Confidence":".30"},


              
               }

    return jsonify(defects)

@app.route('/employees/<type>', methods=['GET'])
def take_building_photo(type):

    # format is jobrole:name
    onsite = {"Manager":"Sidney"}


    all = {"Manager":"Sidney"}

    response = jsonify({})
    if type == "onsite":
        response = jsonify(onsite)
    elif type == "all":
        response = jsonify(all)

    return response

@app.route('/photo_of_wall/<type>', methods=['GET'])
def take_wall_photo(type):
    image_binary = open(APP_ROOT + "/static/walls/" + type + ".png", 'rb').read()
    response = make_response(image_binary)
    response.headers.set('Content-Type', 'image/jpeg')
    return response

if __name__ == '__main__':
    app.run(threaded=True, host="0.0.0.0", port=5003)